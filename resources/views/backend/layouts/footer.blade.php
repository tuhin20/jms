<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @php echo date("Y") ;  @endphp &copy; Developed By <a href="https://www.anhgroupbd.com/"><span style="font-style: italic;">ANH Group</span></a> 
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    
                </div>
            </div>
        </div>
    </div>
</footer>