<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="{{ asset('assets/js/vendor.min.js') }}"></script>

<!-- Plugin js-->
<script src="{{ asset('assets/libs/parsleyjs/parsley.min.js') }}"></script>

<!-- Validation init js-->
<script src="{{ asset('assets/js/pages/form-validation.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/libs/footable/footable.all.min.js') }}"></script>

<!-- Init js -->
<script src="{{ asset('assets/js/pages/foo-tables.init.js') }}"></script>
 <!-- Summernote js -->
 <script src="{{ asset('assets/libs/summernote/summernote-bs4.min.js') }}"></script>

 <!-- Init js -->
 <script src="{{ asset('assets/js/pages/form-summernote.init.js') }}"></script>
 
 <!-- Select2 js-->
 <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
 <!-- Dropzone file uploads-->
 <script src="{{ asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
 <script src="{{ asset('assets/js/pages/add-product.init.js') }}"></script>
 <script src="{{ asset('assets/libs/summernote/summernote-bs4.min.js') }}"></script>

 <!-- Init js -->
 <script src="{{ asset('assets/js/pages/form-summernote.init.js') }}"></script>
 @yield('js')