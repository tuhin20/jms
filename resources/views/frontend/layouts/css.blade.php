<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>ANH Group</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
  <meta content="Coderthemes" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="{{ asset('frontend/assets/images/logo/ANH_Group_logo_white.jpg') }}">

  <!-- App css -->
  <link href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('frontend/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('frontend/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('frontend/assets/libs/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css" />

  <link href="{{ asset('frontend/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

  <link href="{{ asset('frontend/assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- custom css -->
  <link href="{{ asset('frontend/assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
  @yield('style')

  <!-- App css -->

</head>